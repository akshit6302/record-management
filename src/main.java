import java.io.*;
import java.util.Scanner;

public class main{


    static BufferedReader br = new BufferedReader(new InputStreamReader(System.in));

    public void addRecords() throws IOException {

        PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("data.txt",true)));
        String name;
        String address;
        String dob;
        int age;
        long telephoneNo;
        String sNo;
        boolean addMore = false;

        do
        {
            System.out.print("\nEnter name: ");
            name = br.readLine();


            System.out.print("Address: ");
            address = br.readLine();


            System.out.print("Date of Birth (dd/mm/yy) : ");
            dob = br.readLine();

            System.out.print("Age: ");
            age = Integer.parseInt(br.readLine());

            System.out.print("Telephone No.: ");
            telephoneNo = Long.parseLong(br.readLine());

            pw.println(name);
            pw.println(address);
            pw.println(dob);
            pw.println(age);
            pw.println(telephoneNo);

            System.out.print("\nRecords added successfully !\n\nDo you want to add more records ? (y/n) : ");
            sNo = br.readLine();
            if(sNo.equalsIgnoreCase("y"))
            {
                addMore = true;
                System.out.println();
            }
            else
                addMore = false;
        }
        while(addMore);
        pw.close();
        showMenu();
    }

    public void readRecords() {
        try
        {

            BufferedReader file = new BufferedReader(new FileReader("data.txt"));
            String name;
            int i=1;

            while((name = file.readLine()) != null)
            {
                System.out.println("S.No. : " +(i++));
                System.out.println("-------------");
                System.out.println("\nName: " +name);
                System.out.println("Address: "+file.readLine());
                System.out.println("Date of Birth : "+file.readLine());
                System.out.println("Age: "+Integer.parseInt(file.readLine()));
                System.out.println("Tel. No.: "+Long.parseLong(file.readLine()));

            }
            file.close();
            showMenu();
        }
        catch(Exception e)
        {
            System.out.println(e);
        }
    }
    public void clear() throws IOException {

        PrintWriter pw = new PrintWriter(new BufferedWriter(new FileWriter("data.txt")));
        pw.close();
        System.out.println("\nAll Records cleared successfully !");

        showMenu();
    }




    public void showMenu() throws IOException {
        System.out.print("1 : Add Records\n2 : Display Records\n3 : Clear All Records\n4 : Exit\n\nYour Choice : ");
        int choice = Integer.parseInt(br.readLine());
        switch(choice)
        {
            case 1:
                addRecords();
                break;
            case 2:
                readRecords();
                break;
            case 3:
                clear();
                break;
            case 4:
                System.exit(1);
                break;
            default:
                System.out.println("\nInvalid Choice !");
                break;
        }
    }


    public void guestShowMenu() throws IOException {
        System.out.print("1 : Display Records\n2 : Exit\n\nYour Choice : ");
        int choice = Integer.parseInt(br.readLine());
        switch(choice)
        {
            case 1:
                readRecords();
                break;
            case 2:
                System.exit(1);
                break;
            default:
                System.out.println("\nInvalid Choice !");
                break;
        }
    }



    public static void main(String args[]) throws IOException{
        main call = new main();
        Scanner scanner = new Scanner(System.in);
        System.out.println("enter username");
        String username = scanner.nextLine();
        System.out.println("enter password");
        String password =scanner.nextLine();
        UserAccount check = new UserAccount(username,password);
        if(check.checkLogin()){

            call.showMenu();
        }
        else{
            System.out.println("wrong login info: \n"+" press y for try again or n for guest access");
            String  in = scanner.nextLine();
            if (in.equalsIgnoreCase("y")){
                System.out.println("enter username");
                 username = scanner.nextLine();
                System.out.println("enter password");
                 password =scanner.nextLine();
                 check = new UserAccount(username,password);
                if(check.checkLogin()){

                    call.showMenu();
                }
                else{
                    System.out.println("wrong info again taking to guest access");
                    call.guestShowMenu();
                }
            }
        }


    }
}